import React, {Component} from 'react';
import { Button } from './Button';
import axios from "axios";
import _ from 'lodash';

export class Details extends Component {
    state = {
        wikiDetails: {},
        wikiDescription: '',
    };
    componentDidMount() {
        const { selectedPlace } = this.props;
        const lat = _.get(selectedPlace, 'geometry.location.lat');
        const long = _.get(selectedPlace, 'geometry.location.lng');
        axios.get(
            `https://en.wikipedia.org/w/api.php?action=query&origin=*&format=json&generator=geosearch&prop=coordinates|pageimages&ggscoord=${lat()}|${long()}`
        ).then(res => {
            const pageTitle = _.chain(res)
                .get('data.query.pages')
                .values()
                .first()
                .get('title')
                .value();
            this.setState({ wikiDetails: _.chain(res)
                    .get('data.query.pages')
                    .values()
                    .first().value() });
            axios.get(
                `https://en.wikipedia.org/w/api.php?format=json&origin=*&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=${encodeURIComponent(pageTitle.trim())}`
            ).then(res2 => this.setState({
                wikiDescription: _.chain(res2).get('data.query.pages').values().first().get('extract').value(),
            })).catch(err => console.log(err));
        }).catch(err => console.log(err));
    }
    render() {
        const { selectedPlace } = this.props;
        const { wikiDetails, wikiDescription } = this.state;
        console.log( this.state, this.props);
        return (
            <div className="col">
                <hr/>
                <h3>{selectedPlace.formatted_address}</h3>
                <hr/>
                {
                    selectedPlace.website &&
                    <a href={selectedPlace.website} className="btn-link" target="_blank">Website</a>
                }
                <br />
                <br />
                <Button text="discounts" handleClick={() =>
                    window.location.href = `https://www.google.com/search?q=${_.get(wikiDetails, 'title')}+discounts+travel`} />
                <p>{wikiDescription}</p>
                {
                    _.get(selectedPlace, 'photos') && _.map(_.get(selectedPlace, 'photos'), image =>
                        <img src={image.getUrl()} className="img-fluid"/>
                    )
                }
                <br />
            </div>
        )
    }
};