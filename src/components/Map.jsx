import React from 'react';
import { compose, withProps } from "recompose";
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker
} from "react-google-maps";
import _ from "lodash";

export const Map = compose(
    withProps({
        /**
         * Note: create and replace your own key in the Google console.
         * https://console.developers.google.com/apis/dashboard
         * The key "AIzaSyBkNaAGLEVq0YLQMi-PYEMabFeREadYe1Q" can be ONLY used in this sandbox (no forked).
         */
        googleMapURL:
            "https://maps.googleapis.com/maps/api/js?key=AIzaSyBw6Uagn4Hc-6wisooFzycxHUdjHem4248&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `calc(100vh - 72px)` }} />,
        mapElement: <div style={{ height: `100%` }} />
    }),
    withScriptjs,
    withGoogleMap
)(props => {
    const lat = _.get(props.selectedPlace, 'geometry.location.lat');
    const long = _.get(props.selectedPlace, 'geometry.location.lng');
    const coords = {
        lat: (lat ? lat() : 50.06465009999999),
        lng: (long ? long() : 19.94497990000002),
    };
    return (
        <GoogleMap defaultZoom={8} defaultCenter={coords} center={coords}>
            {props.isMarkerShown && (
                <Marker position={coords} />
            )}
        </GoogleMap>
    );
});