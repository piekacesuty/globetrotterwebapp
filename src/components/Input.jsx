import React from 'react';
import { Field } from 'formik';

const Input = ({ errors, touched, validate, classes, type, name }) => (
	<div className={`form-group ${classes}`}>
		{ name && <label htmlFor={name}>{name}</label> }
		<Field name={name} validate={validate} className="form-control" type={type} />
		{ errors && touched && <div className="invalid-feedback">{errors}</div> }
	</div>
);

export default Input;
