import React from 'react';
import _ from 'lodash';
import { SIDEBAR_MODES } from "../constants";
import { SearchBox } from "./SearchBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCompass } from "@fortawesome/free-solid-svg-icons";

export const Navbar = ({signOut, changeMode, changePlace}) => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="#">
            <strong><FontAwesomeIcon icon={faCompass} /> Globetrotter App</strong>
        </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"/>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">
            <ul className="navbar-nav my-2 ml-2">
                {
                    _.map(_.filter(_.values(SIDEBAR_MODES), mds=> mds !== SIDEBAR_MODES.DETAILS), mode =>
                        <li className="nav-item" key={mode}>
                            <a className="nav-link" href="#" onClick={() => changeMode(mode)}>{mode}</a>
                        </li>
                    )
                }
                <li className="nav-item">
                    <a className="nav-link" onClick={signOut}>logout</a>
                </li>
            </ul>
            <SearchBox onValueChange={value => changePlace(value)} showPlaces={false} />
        </div>
    </nav>
);