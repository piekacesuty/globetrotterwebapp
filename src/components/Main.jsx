import React from 'react';
import { Navbar } from "./Navbar";
import { Map } from "./Map";
import { SidePanel } from "./SidePanel";

export const Main = ({signOut, changeMode, changePlace, mode, userDetails, selectedPlace, discounts, ideas, places}) => <div>
    <Navbar signOut={signOut} changeMode={changeMode} changePlace={changePlace}/>
    <Map selectedPlace={selectedPlace} isMarkerShown={true}/>
    { mode && <SidePanel mode={mode}
                         userDetails={userDetails}
                         selectedPlace={selectedPlace}
                         changeMode={changeMode}
                         discounts={discounts}
                         ideas={ideas}
                         places={places}
    />}
</div>;