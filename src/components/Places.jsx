import React from 'react';
import _ from 'lodash';

export const Places = ({ places }) => _.map(places, place =>
    <div className="discount" key={place.pageid}>
      <img src={_.get(place, 'thumbnail.source')} alt="thumbnail"/>
      <h2>{place.title}</h2>
    </div>
);