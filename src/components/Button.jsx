import React from 'react';

export const Button = ({ text, handleClick }) => (
    <button
        className="btn btn-primary"
        onClick={handleClick}
        type="button"
    >
        {text}
    </button>
);
