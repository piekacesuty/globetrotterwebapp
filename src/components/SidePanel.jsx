import React from 'react';
import {Profile} from "./Profile";
import {Discounts} from "./Discounts";
import {SIDEBAR_MODES} from "../constants";
import {Ideas} from "./Ideas";
import {Details} from "./Details";
import {Places} from "./Places";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

export const SidePanel = ({mode, userDetails, changeMode, selectedPlace, discounts, ideas, places}) => <div className="panelRight pt-3" style={{
    position: 'fixed',
    top: 72,
    right: 0,
    width: 350,
    height: 'calc(100vh - 72px)',
    backgroundColor: 'white',
    boxShadow: '0 0 5px rgba(0, 0, 0, 0.33)',
}}>
    <div className="col mb-2">
        <strong className="text-capitalize h2">{mode}</strong>
        <button className="btn btn-danger float-right" type="button" onClick={() => changeMode(null)}>
            <FontAwesomeIcon icon={faTimes} />
        </button>
    </div>
    {mode === SIDEBAR_MODES.PROFILE &&
        <Profile userDetails={userDetails} />
    }
    {mode === SIDEBAR_MODES.DISCOUNTS &&
        <Discounts discounts={discounts} />
    }
    {mode === SIDEBAR_MODES.IDEAS &&
        <Ideas ideas={ideas} />
    }
    {mode === SIDEBAR_MODES.DETAILS &&
        <Details selectedPlace={selectedPlace} />
    }
    {mode === SIDEBAR_MODES.PLACES &&
        <Places places={places} />
    }
</div>;