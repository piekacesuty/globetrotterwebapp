import React from 'react';

export const Profile = ({userDetails}) => <div className="col">
    <img src={userDetails.photoURL} className="img-fluid"/>
    <hr/>
    <h3>{userDetails.displayName}</h3>
    <br/>
    <h5>{userDetails.email}</h5>
</div>;