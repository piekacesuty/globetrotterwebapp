import React, { Component } from 'react';
import _ from 'lodash';
import v8n from 'v8n';
import Input from '../components/Input';
import { Formik, Form } from 'formik';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCompass } from '@fortawesome/free-solid-svg-icons';
import { faGoogle, faFacebook } from '@fortawesome/free-brands-svg-icons';
import firebase from 'firebase';;

const ERROR_MESSAGES = {
    REQUIRED: 'This field is requred',
    IS_INVALID: 'field value is invalid',
};

const validateLogin = value => {
    let error;
    if (!value) {
        error = ERROR_MESSAGES.REQUIRED;
    } else if(!v8n().string().pattern(/[a-z]+/).test(value)) {
        error = `Login ${ERROR_MESSAGES.IS_INVALID}`;
    }
    return error;
};

const LoginForm = () => (
    <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={values =>
            firebase.auth().signInWithEmailAndPassword(values.email, values.password)
            .catch(error =>alert(error.code + ' ' + error.message))}
    >
        {
            ({ errors, touched, isValidating }) => (
                <Form>
                    <Input validate={validateLogin} errors={errors.email} touched={touched.email} name="email"/>
                    <Input validate={validateLogin} errors={errors.password} touched={touched.password} type="password" name="password" />
                    <button className="btn btn-primary btn-block" onClick={_.noop()} type="submit">Log in</button>
                </Form>
            )
        }
    </Formik>
);

const RegisterForm = () => (
    <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={values => {
            let userRef = firebase.database().ref('/users');
            firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
            .then(()=> {
                userRef.once('value').then(snapshot => {
                    userRef.child(snapshot.val().length).set({
                        name: values.name,
                        surname: values.surname,
                        location: values.location,
                        email: values.email
                    });})
            }).catch(error =>alert(error.code + ' ' + error.message));
        }}
    >
        {
            ({ errors, touched, isValidating }) => (
                <Form>
                    <Input validate={validateLogin} errors={errors.name} touched={touched.name} name="name" type="text"/>
                    <Input validate={validateLogin} errors={errors.surname} touched={touched.surname} name="surname" type="text"/>
                    <Input validate={validateLogin} errors={errors.password} touched={touched.password} type="password" name="password" />
                    <Input validate={validateLogin} errors={errors.email} touched={touched.email} type="email" name="email" />
                    <Input errors={errors.location} touched={touched.location} type="text" name="location" />
                    <button className="btn btn-primary btn-block" onClick={_.noop()} type="submit">Register</button>
                </Form>
            )
        }
    </Formik>
);

export class Login extends Component {
    state = {
        logIn: true,
    };
    handleChangeFormMode = () => this.setState({ logIn: !this.state.logIn });
    render() {
        return (
            <div className="container-fluid">
                <div className="row justify-content-center">
                    <div className="col col-sm-7 col-md-5 col-lg-3 m-5">
                        <small>Login to</small>
                        <h1><strong><FontAwesomeIcon icon={faCompass} /> Globetrotter App</strong></h1>
                        <p>Your tripping best friend!</p>
                        <br />
                        { this.state.logIn ? <LoginForm /> : <RegisterForm /> }
                        <button
                            className="btn btn-link btn-block"
                            onClick={this.handleChangeFormMode}
                            type="button"
                        >
                            { this.state.logIn ?
                                <span>Not our user yet? <strong>Register here!</strong></span> :
                                <span><strong>Log in here</strong>, if you have an account already</span>
                            }
                        </button>
                        <br />
                        <hr />
                        <br />
                        <div>
                            <button type="button" onClick={this.props.signInGoogle} className="btn btn-secondary btn-block"><FontAwesomeIcon icon={faGoogle} /> Sign in with Google</button>
                            <button type="button" onClick={this.props.signInFacebook} className="btn btn-secondary btn-block"><FontAwesomeIcon icon={faFacebook} /> Sign in with Facebook</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}