import React from 'react';
import _ from 'lodash';

export const Ideas = ({ ideas }) => (
    _.map(ideas, (d, i) =>
      <div className="discount m-3">
        <span>{d.category}</span>
        <h2>{d.title}</h2>
        <p>{d.description}</p>
      </div>
    )
);