export const SIDEBAR_MODES = {
    PROFILE: 'profile',
    DISCOUNTS: 'discounts',
    IDEAS: 'ideas',
    DETAILS: 'details',
    PLACES: 'places'
};