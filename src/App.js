import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import _ from 'lodash';
import { Main } from "./components/Main";
import {  Login } from "./components/Login";
import withFirebaseAuth from 'react-with-firebase-auth'
import * as firebase from 'firebase';
import 'firebase/auth';
import firebaseConfig from './firebaseConfig';
import {SIDEBAR_MODES} from "./constants";
import axios from "axios";

const firebaseApp = firebase.initializeApp(firebaseConfig);

const firebaseAppAuth = firebaseApp.auth();
const providers = {
    googleProvider: new firebase.auth.GoogleAuthProvider(),
    facebookProvider: new firebase.auth.FacebookAuthProvider()
};

class App extends Component {
    state = {
        selectedPlace: null,
        sidebarMode: null,
        ideas: [],
        discounts: [],
        places:{}
    };

    componentDidMount(){
        let testRef = firebase.database().ref('/ideas');
        testRef.on('value', snapshot => {
            let val = _.compact(snapshot.val());
            this.setState({
                ideas: val
            });
        });

        let discountsRef = firebase.database().ref('/discounts');
        discountsRef.on('value', snapshot => {
            let val = _.compact(snapshot.val());
            this.setState({
                discounts: val
            });
        });
        this.getMonuments(50.06465009999999, 19.94497990000002)

    }

    simpleObject(obj) {
        let places = []
        for(const key in obj){
            places.push(obj[key]);
        }
        return places
    }

    getMonuments(lat, long){
        console.log(lat, long)
        axios.get(`https://en.wikipedia.org/w/api.php?action=query&origin=*&format=json&generator=geosearch&prop=coordinates|pageimages&ggscoord=${lat}|${long}`).then(res => this.setState({places: this.simpleObject(res.data.query.pages)})).catch(err => console.log(err))
    }

    handleSidebarModeChange = mode => this.setState({ sidebarMode: mode });
    handlePlaceChange = places => {
        let place = _.first(places);
        this.setState({ selectedPlace: place, sidebarMode: SIDEBAR_MODES.DETAILS })
        const lat = _.get(place, 'geometry.location.lat');
        const long = _.get(place, 'geometry.location.lng');
        this.getMonuments(lat(), long());
    };

    render() {
      const {
          user,
          signOut,
          signInWithGoogle,
          signInWithFacebook
      } = this.props;
      return (
        <div className="App">
            { user ? <Main signOut={signOut}
                  changePlace={this.handlePlaceChange}
                  changeMode={this.handleSidebarModeChange}
                  userDetails={user}
                  discounts={this.state.discounts}
                  mode={this.state.sidebarMode}
                  ideas={this.state.ideas}
                  places={this.state.places}
                              selectedPlace={this.state.selectedPlace}/> :
                        <Login signInGoogle={signInWithGoogle} signInFacebook={signInWithFacebook} /> }
        </div>
    );
  }
}

export default withFirebaseAuth({
    providers,
    firebaseAppAuth,
})(App);
